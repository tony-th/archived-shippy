# detect which services is changed and build image for thoses

changed=`git diff --name-only $CI_COMMIT_BEFORE_SHA | sort -u | awk 'BEGIN {FS="/"} {print $1}' | uniq | grep '\-service$' | sed 's/\-service$//'`

# login to registry
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

for service in $changed; do
    IMAGE="${CI_REGISTRY_IMAGE}/${service}:${CI_COMMIT_REF_SLUG}"
    docker build --build-arg SERVICE=$service -t  $IMAGE .
    docker push $IMAGE
done
