# We use the official golang image, which contains all the correct build tools and libraries.
# Notice `as builder`, this gives this container a name that we can reference later on.
FROM golang:alpine as builder

ARG SERVICE
RUN apk --no-cache add git

# Set our workdir to our current service in the gopath
WORKDIR /shippy

ADD go.* /shippy/
RUN go mod download

# Copy the current code into our workdir
COPY . .

# Build the binary, with a few flags which allow us to run this in Alpine
RUN CGO_ENABLED=0 GOOS=linux go build -o app "./${SERVICE}-service"

# Here we're using a second FROM statement, which is strange,
# but this tells Docker to start a new build process with this image
FROM alpine:latest

# Security related package, good to have
RUN apk --no-cache add ca-certificates

# Here, instead of copying the binary from our host machine,
# we pull the binary from the container named `builder`, within
# this build context. This reaches into our previous image, finds
# the binary we build, and pulls it into this container. Amazing!
COPY --from=builder /shippy/app /app

CMD ["./app"]
